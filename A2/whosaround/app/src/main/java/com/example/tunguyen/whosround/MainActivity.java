package com.example.tunguyen.whosround;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.tunguyen.whosround.R;

import java.util.ArrayList;

//OnItemSelectedListener is used for Spinners, and OnItemClickListener is used for ListViews.
public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener, AdapterView.OnItemClickListener{

    private Spinner mSpinner, mSpinner2;
    private ListView mListView;
    ArrayAdapter<String> spinnerAdaper, spinnerAdapter2, listViewAdapter;
    // Array of choices
    private String[] minutes = {"15 min","30 min", "1 hr", "12 hr", "24 hr"};
    private String[] miles = {"2 mi", "15 mi", "50 mi", "100 mi"};
//    private String[] people = {"person1", "person2", "person3", "person4", "person5"};

    // Find this in your developer console
    private static final String APP_ID = "0dfe9797f15da547b66830f30a6b58c0";
    // Find this in your developer console
    private static final String API_KEY = "e49a29df1d2e442985d841f23d9e412e";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSpinner = (Spinner) findViewById(R.id.spinner);
        mSpinner2 = (Spinner) findViewById(R.id.spinner2);
        // Application of the Array to the Spinner
        spinnerAdaper = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, minutes);
        spinnerAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, miles);
        // The drop down view
        spinnerAdaper.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(spinnerAdaper);
        mSpinner.setOnItemSelectedListener(this);

        spinnerAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner2.setAdapter(spinnerAdapter2);
        mSpinner2.setOnItemSelectedListener(this);

        //create basic ArrayAdapter
        mListView = (ListView) findViewById(R.id.listView);
//        listViewAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, people);
//        mListView.setAdapter(listViewAdapter);
        mListView.setOnItemClickListener(this);

        //Using a Custom ArrayAdapter
        // Construct the data source
        ArrayList<Person> arrayOfPeople = new ArrayList<Person>();
        // Create the adapter to convert the array to views
        UsersAdapter adapter = new UsersAdapter(this, arrayOfPeople);
        // Attach the adapter to a ListView
        mListView.setAdapter(adapter);
        // Add item to adapter
        Person mPerson = (Person)getIntent().getSerializableExtra(MapsActivity.SER_KEY0);
        Person person2 = (Person)getIntent().getSerializableExtra(MapsActivity.SER_KEY1);
        Person person3 = (Person)getIntent().getSerializableExtra(MapsActivity.SER_KEY2);
        Person person4 = new Person("Person","4");
        Person person5 = new Person("Person","5");
        Person person6 = new Person("Person","6");
        adapter.add(mPerson);
        adapter.add(person2);
        adapter.add(person3);
        adapter.add(person4);
        adapter.add(person5);
        adapter.add(person5);
        adapter.add(person6);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        TextView myText = (TextView) view;
//        Toast.makeText(this, "You selected "+myText.getText(), Toast.LENGTH_SHORT).show();
    }

    //onNothingSelected is called when you current selection disappears due to some event like touch getting
    //activated or the adapter becomes empty
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        LinearLayout myText = (LinearLayout) view;
//        Toast.makeText(this, "You selected "+position, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, "ryanefendy95@gmail.com");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Hi, this was sent from my app");
        intent.putExtra(Intent.EXTRA_TEXT, "Hey, what's up, how are you doing? this is my first email message");
        intent.setType("message/rfc822");
        Intent chooser=Intent.createChooser(intent, "Send Email");
        startActivity(chooser);

    }

    public class UsersAdapter extends ArrayAdapter<Person> {
        public UsersAdapter(Context context, ArrayList<Person> people) {
            super(context, 0, people);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Get the data item for this position
            Person person = getItem(position);
            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_template, parent, false);
            }
            // Lookup view for data population
            TextView tvMiles = (TextView) convertView.findViewById(R.id.tvMiles);
            TextView tvPerson = (TextView) convertView.findViewById(R.id.tvPerson);
            // Populate the data into the template view using the data object
            tvMiles.setText(person.getMiles());
            tvPerson.setText(person.getFirstName() + " " + person.getLastName());
            // Return the completed view to render on screen
            return convertView;
        }
    }
}
