package com.example.tunguyen.whosround;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.Response;
import com.cloudmine.api.CMApiCredentials;
import com.cloudmine.api.CMGeoPoint;
import com.cloudmine.api.CMObject;
import com.cloudmine.api.SearchQuery;
import com.cloudmine.api.db.LocallySavableCMObject;
import com.cloudmine.api.persistance.ClassNameRegistry;
import com.cloudmine.api.rest.response.CMObjectResponse;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private GoogleApiClient mLocationClient;
    private GPSTracker mGPSTracker;
    private double mLatitude, mLongitude;
    LatLng currentLocation;
    public static final String TAG = MainActivity.class.getSimpleName();

    // Find this in your developer console
    private static final String APP_ID = "0dfe9797f15da547b66830f30a6b58c0";
    // Find this in your developer console
    private static final String API_KEY = "e49a29df1d2e442985d841f23d9e412e";
    //Master Key Access
    private static final String MASTER_KEY = "";

    static {
        ClassNameRegistry.register(Person.CLASS_NAME, Person.class);
    }

    private List<Person> listOfPeople = new ArrayList<Person>();
    public final static String SER_KEY0 = "person0";
    public final static String SER_KEY1 = "person1";
    public final static String SER_KEY2 = "person2";
    private static final String apiKey = "AIzaSyAsN80BS-4iMWTpxW1KcjigGgeq4zzHGyQ";
    private Bundle mBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (initMap()) {
            mGPSTracker = new GPSTracker(this);
            if (mGPSTracker.canGetLocation()) {
                mLatitude = mGPSTracker.getLatitude();
                mLongitude = mGPSTracker.getLongitude();
                //Drexel lat: 38.479459, long: -94.608566
                Toast.makeText(
                        getApplicationContext(),
                        "Your Location is -\nLat: " + mLatitude + "\nLong: "
                                + mLongitude, Toast.LENGTH_LONG).show();
                createMarker(mLatitude, mLongitude, "My Location");
            } else {
                mGPSTracker.showSettingsAlert();
            }
        } else {
            Toast.makeText(this, "Map not connected!", Toast.LENGTH_SHORT).show();
        }

        // initialize CloudMine library
        CMApiCredentials.initialize(APP_ID, API_KEY, getApplicationContext());


        //Geo search
        LocallySavableCMObject.searchObjects(this, SearchQuery.filter("location").near(37.28, 75.90).searchQuery(),
                new Response.Listener<CMObjectResponse>() {
                    @Override
                    public void onResponse(CMObjectResponse response) {
                        JsonParser jp = new JsonParser(); //create the Json
                        JsonElement root = null;
                        JsonObject rootobj = null;
//                        Log.i(TAG, response.getMessageBody());
                        for (CMObject people : response.getObjects()) {
//                            Log.i(TAG, people.asKeyedObject());
                            int locationIndex = people.asKeyedObject().indexOf(':');
//                            Log.i(TAG, people.asKeyedObject().substring(locationIndex+1));
                            root = jp.parse(people.asKeyedObject().substring(locationIndex + 1));
                            rootobj = root.getAsJsonObject();
                            String firstName = rootobj.get("firstName").getAsString();
                            String lastName = rootobj.get("lastName").getAsString();
                            String latitude = rootobj.get("location").getAsJsonObject().get("latitude").getAsString();
                            String longitude = rootobj.get("location").getAsJsonObject().get("longitude").getAsString();
                            Person p = new Person(firstName, lastName);
//                            p.setMiles(miles);
                            CMGeoPoint location = new CMGeoPoint(Double.parseDouble(longitude), Double.parseDouble(latitude));// lon, lat
                            p.setLocation(location);
                            listOfPeople.add(p);
                            createMarker(p.getLocation().getLatitude(), p.getLocation().getLongitude(), firstName + " " + lastName);
                        }
                    }
                });
    }

    private void createMarker(double latitude, double longitude) {
        //(double latitude, double longitude)
        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions options = new MarkerOptions()
                .position(latLng);
        mMap.addMarker(options);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
    }

    private void createMarker(double latitude, double longitude, String locality) {
        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions options = new MarkerOptions()
                .title(locality)
                .position(latLng);
        mMap.addMarker(options);
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //Add menu handling code
        switch (id) {
            case R.id.item1:
                Toast.makeText(getApplicationContext(), "item1", Toast.LENGTH_SHORT).show();
//                Intent intent = new Intent(this, MainActivity.class);
//                intent.putExtra("person1", "value");
//                startActivity(intent);
                this.SerializeMethod();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

//    passing object by Intent via Serializable
    public void SerializeMethod() {
        Intent mIntent = new Intent(this, MainActivity.class);
        Bundle mBundle = new Bundle();
        Person person0 = new Person(listOfPeople.get(0).getFirstName(), listOfPeople.get(0).getLastName());
        person0.setMiles(listOfPeople.get(0).getMiles());
        Person person1 = new Person(listOfPeople.get(1).getFirstName(), listOfPeople.get(1).getLastName());
        Person person2 = new Person(listOfPeople.get(2).getFirstName(), listOfPeople.get(2).getLastName());
        mBundle.putSerializable(SER_KEY0, person0);
        mBundle.putSerializable(SER_KEY1, person1);
        mBundle.putSerializable(SER_KEY2, person2);
        mIntent.putExtras(mBundle);
        startActivity(mIntent);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    private boolean initMap() {
        if (mMap == null) {
            SupportMapFragment mapFragment =
                    (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mMap = mapFragment.getMap();
        }
        return (mMap != null);
    }

    public class MyBigTask extends AsyncTask<Void, Void, String> {

        String latitude, longitude;

        public MyBigTask(String latitude, String longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        @Override
        protected String doInBackground(Void... params) {
            //1. create okHttp Client object
            OkHttpClient client = new OkHttpClient();

            //2. Define reqest being sent to the server
            RequestBody postData = new FormBody.Builder()
                    .add("type", "json")
                    .build();

            Request request = new Request.Builder()
//                    .url("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=39.956821%20-75.189804&destinations=39.925599%20-75.176236%20&key=AIzaSyAsN80BS-4iMWTpxW1KcjigGgeq4zzHGyQ")
                    .url("https://maps.googleapis.com/maps/api/distancematrix/json?units=" +
                            "imperial&origins="+mLatitude+"%20"+mLongitude+"%20&destinations="+latitude+"%20"+longitude+"%20&" +
                            "key=AIzaSyAsN80BS-4iMWTpxW1KcjigGgeq4zzHGyQ")
                    .post(postData)
                    .build();

            //3. Transport the request and wait for response to process next
            okhttp3.Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String result = null;
            try {
                result = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JsonParser jp = new JsonParser(); //create the Json
            JsonElement root = jp.parse(result); //read the data using InputStreamReader
            JsonObject rootobj = root.getAsJsonObject();
            String distance = rootobj.get("rows").getAsJsonArray().get(0).getAsJsonObject().get("elements").getAsJsonArray().get(0).getAsJsonObject().get("distance").getAsJsonObject().get("text").getAsString();

            return distance;
        }


    }

}
