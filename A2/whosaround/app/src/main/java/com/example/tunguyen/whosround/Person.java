package com.example.tunguyen.whosround;

import com.cloudmine.api.db.LocallySavableCMObject;
import com.cloudmine.api.CMGeoPoint;

import java.io.Serializable;

public class Person extends LocallySavableCMObject implements Serializable {
    public static final String CLASS_NAME = "Person";
    private String firstName;
    private String lastName;
    private String miles;
    private Double latitude, longitude;
    private CMGeoPoint location; //Geolocation Tagging and Searching
    private static final long serialVersionUID = 465489764; //Added serialVersionUID variable


    //there must be a no-args constructor for deserializing to work
    Person(){ super(); }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.miles = "0.0";
        this.location = null;
        this.latitude = 0.0;
        this.longitude = 0.0;
    }

    //Your getter and setters determine what gets serialized and what doesn't
    public String getFirstName() {return firstName;}
    public void setFirstName(String firstName) {this.firstName = firstName;}
    public String getLastName() {return lastName;}
    public void setLastName(String lastName) {this.lastName = lastName;}

    public CMGeoPoint getLocation() {
        return location;
    }

    public void setLocation(CMGeoPoint location) {
        this.location = location;
    }

    public String getMiles() {
        return miles;
    }

    public void setMiles(String miles) {
        this.miles = miles;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}

