import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth;
import com.temboo.Library.Utilities.Encoding.Base64Decode;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;
import org.apache.commons.codec.binary.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;


public class Dropbox{

    public static void main(String[] args) throws TembooException {
        String appKey, appSecret, callbackID, OAuthTokenSecret, AccessToken, AccessTokenSecret;
        appKey = "yn6umi1xlt153yy";
        appSecret = "1h0zv4ftzf3jtbe";

        String user, appKeyName, appKeyValue;
        user = "2doo";
        appKeyName = "myFirstApp";
        appKeyValue = "dl23Lt5xivFIMWg8SijqFs8W1MrB3oye";
     
        TembooSession session = null;
        session = new TembooSession(user, appKeyName, appKeyValue);

        InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

        InitializeOAuth.InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

        // Set inputs
        initializeOAuthInputs.set_DropboxAppKey(appKey);
        initializeOAuthInputs.set_DropboxAppSecret(appSecret);

        // Execute 
        InitializeOAuth.InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);

        Scanner user_input = new Scanner(System.in);
        openInBrowser(initializeOAuthResults.get_AuthorizationURL());
        callbackID = user_input.nextLine().trim();
        callbackID = initializeOAuthResults.get_CallbackID().trim();
        OAuthTokenSecret = initializeOAuthResults.get_OAuthTokenSecret().trim();
        System.out.println("callbackID: " + callbackID);
        System.out.println("OAuthTokenSecret: " + OAuthTokenSecret);

        FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

        // Get an InputSet object for the choreo
        FinalizeOAuth.FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

        // Set inputs
        finalizeOAuthInputs.set_OAuthTokenSecret(OAuthTokenSecret);
        finalizeOAuthInputs.set_DropboxAppSecret(appSecret);
        finalizeOAuthInputs.set_DropboxAppKey(appKey);
        finalizeOAuthInputs.set_CallbackID(callbackID);

        // Execute Choreo
        FinalizeOAuth.FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);

        AccessToken = finalizeOAuthResults.get_AccessToken();
        AccessTokenSecret = finalizeOAuthResults.get_AccessTokenSecret();
        System.out.println("AccessToken: " + AccessToken);
        System.out.println("AccessTokenSecret: " + AccessTokenSecret);

        String path = "/move/";
        String response = GetFile(user, appKeyName, appKeyValue, AccessToken, appKey, AccessTokenSecret, appSecret, path + "__list");
        System.out.println("Encoded data: " + response);
        //returns the specified Base64 encoded string as decoded text

        byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(response);
        System.out.println("Decoding text: " + decoded);
        writeFile(decoded,"__list" );
        //read __list file 
        String str = readFile("__list");
        //parse the content 
        ArrayList<String> files = parseFiles(str);
        ArrayList<String> paths = parsePaths(str);
        System.out.println(files);
        System.out.println(paths);

        //write decoded text 
        for (int i = 0; i < files.size() || i < paths.size(); i++) {
            String text = GetFile(user, appKeyName, appKeyValue, AccessToken, appKey, AccessTokenSecret, appSecret, path + files.get(i));
            String decodedText = Base64Decode(user, appKeyName, appKeyValue, AccessToken, appKey, AccessTokenSecret, appSecret, text);
            writeToFile(decodedText, files.get(i), paths.get(i));
        }

        //remove the line from __list 
        writeToFile("", "__list");
        //deletes __list in Dropbox folder
        DeleteFileOrFolder(user, appKeyName, appKeyValue, AccessToken, appKey, AccessTokenSecret, appSecret, "__list");
        //Uploads __list that is overwritten to Dropbox
        UploadFile(user, appKeyName, appKeyValue, AccessToken, appKey, AccessTokenSecret, appSecret, "__list");
    }
     private static void openInBrowser(String url){
        try {
        //Try to open the url in the default browser.
        java.awt.Desktop.getDesktop().browse(java.net.URI.create(url));
        }catch (java.io.IOException e) {
        System.out.println(e.getMessage());
        }catch(java.awt.HeadlessException e){
         //The browser could not be opened. Prompt the user to manually navigate.
        System.out.println("Please navigate to the following page in any web browser: \n"+url);
        }
    }

    //write to file
    public static void writeToFile(String content, String fileName) {
        try {

            File file = new File(fileName);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();

            System.out.println(fileName);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //write to file overloaded method with the ability to specify file path
    public static void writeToFile(String content, String fileName, String filePath) {
        try {

            File file = new File(filePath + "/" + fileName);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();

            System.out.println(fileName);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Decodes Base64 data into octets return an array of bytes
    public static void writeFile(byte[] content, String fileName) {
        FileOutputStream fop = null;
        File file;

        try {

            file = new File(fileName);
            fop = new FileOutputStream(file);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // get the content in bytes
            byte[] contentInBytes = content;

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

            System.out.println("Done");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fop != null) {
                    fop.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //read file, append to string and return the string
    public static String readFile(String file) {

        BufferedReader br = null;
        String str = "";

        try {

            String sCurrentLine;

            br = new BufferedReader(new FileReader(file));

            while ((sCurrentLine = br.readLine()) != null) {
                str += sCurrentLine + "\n";
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return str;
    }

    //parse file name from string
    public static ArrayList<String> parseFiles(String str) {
        List<String> list = new ArrayList<String>(Arrays.asList(str.split("[\\s,;\\n\\t]+")));
        ArrayList<String> files = new ArrayList<String>();

        for (String element : list) {

            if (!element.startsWith("/")) {
                files.add(element);
            }
        }
        return files;
    }

    //parse path name from string
    public static ArrayList<String> parsePaths(String str) {
        List<String> list = new ArrayList<String>(Arrays.asList(str.split("[\\s,;\\n\\t]+")));
        ArrayList<String> paths = new ArrayList<String>();

        for (String element : list) {

            if (element.startsWith("/")) {
                paths.add(element);
            }
        }
        return paths;
    }

    //retrieves the content and metadata for a specified file using a file path you specify, such as "/MyFolder/MyFile.txt".
    public static String GetFile(String user, String appKeyName, String appKeyValue, String AccessToken, String appKey, String AccessTokenSecret, String appSecret, String path) {

        TembooSession session = null;
        try {
            session = new TembooSession(user, appKeyName, appKeyValue);
        } catch (TembooException e) {
            e.printStackTrace();
        }

        // get the file contents from the dropbox file
        GetFile getFileChoreo = new GetFile(session);

        // Get an InputSet object for the choreo
        GetFile.GetFileInputSet getFileInputs = getFileChoreo.newInputSet();

        // Set inputs
        getFileInputs.set_Path(path);
        getFileInputs.set_AccessToken(AccessToken);
        getFileInputs.set_AppKey(appKey);
        getFileInputs.set_EncodeFileContent("true");
        getFileInputs.set_AccessTokenSecret(AccessTokenSecret);
        getFileInputs.set_AppSecret(appSecret);

        // Execute Choreo
        GetFile.GetFileResultSet getFileResults = null;
        try {
            getFileResults = getFileChoreo.execute(getFileInputs);
        } catch (TembooException e) {
            e.printStackTrace();
        }
        return getFileResults.get_Response();
    }

    //Returns the specified Base64 encoded string as decoded text.
    public static String Base64Decode(String user, String appKeyName, String appKeyValue, String AccessToken, String appKey, String AccessTokenSecret, String appSecret, String response) {
        TembooSession session = null;
        try {
            session = new TembooSession(user, appKeyName, appKeyValue);
        } catch (TembooException e) {
            e.printStackTrace();
        }

        Base64Decode base64DecodeChoreo = new Base64Decode(session);

        // Get an InputSet object for the choreo
        Base64Decode.Base64DecodeInputSet base64DecodeInputs = base64DecodeChoreo.newInputSet();

        // Set inputs
        base64DecodeInputs.set_Base64EncodedText(response);

        // Execute Choreo
        Base64Decode.Base64DecodeResultSet base64DecodeResults = null;
        try {
            base64DecodeResults = base64DecodeChoreo.execute(base64DecodeInputs);
        } catch (TembooException e) {
            e.printStackTrace();
        }

        return base64DecodeResults.get_Text();
    }

    public static void CopyFileOrFolder(String user, String appKeyName, String appKeyValue, String AccessToken, String appKey, String AccessTokenSecret, String appSecret, ArrayList<String> files, ArrayList<String> paths) {
        TembooSession session = null;
        try {
            session = new TembooSession(user, appKeyName, appKeyValue);
        } catch (TembooException e) {
            e.printStackTrace();
        }

        //Copies a file or folder to a new location in the Dropbox tree.
        CopyFileOrFolder copyFileOrFolderChoreo = new CopyFileOrFolder(session);

        // Get an InputSet object for the choreo
        CopyFileOrFolder.CopyFileOrFolderInputSet copyFileOrFolderInputs = copyFileOrFolderChoreo.newInputSet();

        for (int i = 0; i < files.size() || i < paths.size(); i++) {

            // Set inputs
            copyFileOrFolderInputs.set_AccessToken(AccessToken);
            copyFileOrFolderInputs.set_AppKey(appKey);
            copyFileOrFolderInputs.set_FromPath("/move/" + files.get(i));
            copyFileOrFolderInputs.set_ToPath(paths.get(i) + "/" + files.get(i));
            copyFileOrFolderInputs.set_AccessTokenSecret(AccessTokenSecret);
            copyFileOrFolderInputs.set_AppSecret(appSecret);

            // Execute Choreo
            try {
                CopyFileOrFolder.CopyFileOrFolderResultSet copyFileOrFolderResults = copyFileOrFolderChoreo.execute(copyFileOrFolderInputs);
            } catch (TembooException e) {
                e.printStackTrace();
            }
        }
    }

    //deletes a Dropbox file or folder that you specify
    public static void DeleteFileOrFolder(String user, String appKeyName, String appKeyValue, String AccessToken, String appKey, String AccessTokenSecret, String appSecret, String file) {
        TembooSession session = null;
        try {
            session = new TembooSession(user, appKeyName, appKeyValue);
        } catch (TembooException e) {
            e.printStackTrace();
        }

        DeleteFileOrFolder deleteFileOrFolderChoreo = new DeleteFileOrFolder(session);

        // Get an InputSet object for the choreo
        DeleteFileOrFolder.DeleteFileOrFolderInputSet deleteFileOrFolderInputs = deleteFileOrFolderChoreo.newInputSet();

        // Set inputs
        deleteFileOrFolderInputs.set_Path("/move/" + file);
        deleteFileOrFolderInputs.set_AccessToken(AccessToken);
        deleteFileOrFolderInputs.set_AppKey(appKey);
        deleteFileOrFolderInputs.set_AccessTokenSecret(AccessTokenSecret);
        deleteFileOrFolderInputs.set_AppSecret(appSecret);

        // Execute Choreo
        try {
            DeleteFileOrFolder.DeleteFileOrFolderResultSet deleteFileOrFolderResults = deleteFileOrFolderChoreo.execute(deleteFileOrFolderInputs);
        } catch (TembooException e) {
            e.printStackTrace();
        }
    }

    //uploads files to a Dropbox account
    public static void UploadFile(String user, String appKeyName, String appKeyValue, String AccessToken, String appKey, String AccessTokenSecret, String appSecret, String file) {
        TembooSession session = null;
        try {
            session = new TembooSession(user, appKeyName, appKeyValue);
        } catch (TembooException e) {
            e.printStackTrace();
        }

        UploadFile uploadFileChoreo = new UploadFile(session);

        // Get an InputSet object for the choreo
        UploadFile.UploadFileInputSet uploadFileInputs = uploadFileChoreo.newInputSet();

        // Set inputs
        uploadFileInputs.set_AccessToken(AccessToken);
        uploadFileInputs.set_FileName("/Users/tunguyen/Desktop/test/" + file);
        uploadFileInputs.set_AppKey(appKey);
        uploadFileInputs.set_AccessTokenSecret(AccessTokenSecret);
        uploadFileInputs.set_Folder("/move/");
        uploadFileInputs.set_AppSecret(appSecret);

        // Execute Choreo
        try {
            UploadFile.UploadFileResultSet uploadFileResults = uploadFileChoreo.execute(uploadFileInputs);
        } catch (TembooException e) {
            e.printStackTrace();
        }
    }
}